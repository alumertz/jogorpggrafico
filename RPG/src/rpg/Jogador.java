package rpg;

import java.util.ArrayList;

public class Jogador {
    private int mana, vida;
    private ArrayList<Magia> vetMagia = new ArrayList<>();
    private final Inventario inventario;
    private String nome;
    
    public Jogador(String nJogador){
        setNome(nJogador);
        setMana(100);
        setVida(70);
        this.inventario = new Inventario();
    }
    public String getNome(){
        return nome;
    }
    public String getNomeItem(int nItem){
        return inventario.getNomeItem(nItem);
    }
    public boolean setItem (int quant, int nItem, String nome){;
        return inventario.setItem(quant, nItem, nome);
    }
    public int getQuantItem (int nItem){
        return inventario.getQuantItem(nItem);
    }
    public String getNomeEquipado(int nItem){
        return inventario.getNomeEquipado(nItem);
    }
    public int getConsumoMana (int magia){
        return this.vetMagia.get(magia).getConsumoMana();
    }
    public String getSTudo (int magia){
        return this.nome +Integer.toString(this.vetMagia.get(magia).getDano()) +Integer.toString(this.vetMagia.get(magia).getConsumoMana());
    }
    public int getDano (int magia){
        return this.vetMagia.get(magia).getDano();
    }
    public void adicionarMagia(String nome, String dano, String consumo){
        this.vetMagia.add(new Magia(nome, Integer.parseInt(dano), Integer.parseInt(consumo)));
    }
    public void setMana(int mana) {
        this.mana = mana;
    }
    public void setVida(int vida) {
        this.vida = vida;
    }
    public String getMagia(int magia){
        return this.vetMagia.get(magia).getNome();
    }
    public void setVetMagia(ArrayList<Magia> vetMagia) {
        this.vetMagia = vetMagia;
    }
    public int getSizeMagia(){
        return vetMagia.size();
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String executarMagia(int magia){
        int cMana, dano;
        if (!vetMagia.isEmpty()){
            cMana = getConsumoMana(magia);
            dano = getDano (magia);
            this.mana -= cMana;
            this.vida -= dano;
            return toString();
        }
        else{
            return "Você ainda não adicionou uma magia!";
        }
        
    }
    
    public void equipar(int item){
        this.inventario.equipar(item);
    }
        
    
    @Override
    public String toString (){
        return "Vida do oponente: "+ this.vida + "\nSua mana: "+ this.mana;
    }
}
