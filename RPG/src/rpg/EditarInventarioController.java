package rpg;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class EditarInventarioController implements Initializable {
    ArrayList<Jogador> vetPersonagem;
    int  quant =0;
    Jogador jogador;
    @FXML
    private TextField nome1,quant1,nome2,quant2,nome3,quant3,nome4,quant4,nome5,quant5;
    
    public void setJogadores(ArrayList<Jogador> vet, Jogador nJogador){
        vetPersonagem = vet;
        jogador = nJogador;
        int quantAnterior = quant;
        
        
        nome1.setText(jogador.getNomeItem(0));
        quant1.setText(Integer.toString(jogador.getQuantItem(0)));
        nome2.setText(jogador.getNomeItem(1));
        quant2.setText(Integer.toString(jogador.getQuantItem(1)));
        nome3.setText(jogador.getNomeItem(2));
        quant3.setText(Integer.toString(jogador.getQuantItem(2)));
        nome4.setText(jogador.getNomeItem(3));
        quant4.setText(Integer.toString(jogador.getQuantItem(3)));
        nome5.setText(jogador.getNomeItem(4));
        quant5.setText(Integer.toString(jogador.getQuantItem(4)));
        
    }
    @FXML
    public void salvar(){
        boolean adicionado = false;
        jogador.setItem(Integer.parseInt(quant1.getText()), 0, nome1.getText());
        jogador.setItem(Integer.parseInt(quant2.getText()), 1, nome2.getText());
        jogador.setItem(Integer.parseInt(quant3.getText()), 2, nome3.getText());
        jogador.setItem(Integer.parseInt(quant4.getText()), 3, nome4.getText());
        jogador.setItem(Integer.parseInt(quant5.getText()), 4, nome5.getText());
        RPG.trocaTela("Menu.fxml", vetPersonagem, jogador);
    }
    @FXML
    public void voltar(){
        RPG.trocaTela("Menu.fxml", vetPersonagem, jogador);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
    }    
    
}
