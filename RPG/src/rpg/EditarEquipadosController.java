package rpg;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class EditarEquipadosController implements Initializable {
    ArrayList<Jogador> vetPersonagem;
    Jogador jogador;
    ArrayList<CheckBox> vetCB = new ArrayList<>();
    
    @FXML
    private Label aviso;
    @FXML
    private CheckBox item1,item2,item3,item4,item5;
    
    
    
    public void setJogadores(ArrayList<Jogador> vet, Jogador nJogador){
        vetPersonagem = vet;
        jogador = nJogador;
        
        item1.setText(jogador.getNomeItem(0));
        item2.setText(jogador.getNomeItem(1));
        item3.setText(jogador.getNomeItem(2));
        item4.setText(jogador.getNomeItem(3));
        item5.setText(jogador.getNomeItem(4));
        
        vetCB.add(item1);
        vetCB.add(item2);
        vetCB.add(item3);
        vetCB.add(item4);
        vetCB.add(item5);
        vetCB.get(0).setSelected(false);
        vetCB.get(1).setSelected(false);
        vetCB.get(2).setSelected(false);
        vetCB.get(3).setSelected(false);
        vetCB.get(4).setSelected(false);
        
        
        for (int x=0;x<5;x++){
            for (int y=0;y<3;y++){
                if (jogador.getNomeItem(x).equals(jogador.getNomeEquipado(y)) && !"[slot vazio]".equals(jogador.getNomeItem(x))){
                    vetCB.get(x).setSelected(true);
                }   
            }
        }
        

    }
    @FXML
    public void salvar(){
        int quant =0;
        for (int x=0;x<5;x++){
            if (vetCB.get(x).isSelected()){
                quant++;
            }
        }
        if (quant>3){
            aviso.setText("Você pode selecionar até 3 itens!");
        }
        else{
            for (int x=0;x<5;x++){
                if (vetCB.get(x).isSelected()){
                    jogador.equipar(x);
                }
                RPG.trocaTela("Menu.fxml", vetPersonagem, jogador);
            }
        }
        
    }
    @FXML
    public void voltar(){
        RPG.trocaTela("Menu.fxml", vetPersonagem, jogador);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
