package rpg;

public class Magia {
   private int consumoMana;
   private int dano;
   private String nome;
   
   public Magia(String nome,int dano, int consumo  ){
       setConsumoMana (consumo);
       setDano(dano);
       setNome(nome);
   }
   
   public int getDano(){
       return this.dano;
   }
   public int getConsumoMana(){
       return this.consumoMana;
   }
   public String getNome(){
       return this.nome;
   }

    public void setConsumoMana(int consumoMana) {
        this.consumoMana = consumoMana;
    }

    public void setDano(int dano) {
        this.dano = dano;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
   
   @Override
   public String toString() {
       return nome + "\t" + dano+ "\t" +consumoMana;
   }
   
    
}
