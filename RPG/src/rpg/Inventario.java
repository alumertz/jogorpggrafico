package rpg;

public class Inventario {
    private final int limiteInventario = 5;
    private final int limiteEquipados = 3;
    private final String[] itens = new String[limiteInventario];
    private final int[] quant = new int[limiteInventario];
    private final String[] itensEquipados = new String [limiteEquipados];
    private int quantItensInventario = 0;
    private int quantItensEquipados =0;
    
    public Inventario(){
        for (int x=0;x<limiteInventario;x++){
            itens[x] = "";
            if (x<3){
                itensEquipados[x] = "";
            }
        }
    }
    
    public void adicionarNovoItem(String item, int quant){
        boolean noInventario = false; 
        for (int x=0; x<limiteInventario && noInventario==false; x++){
            if ("".equals(itens[x])){
                 itens[x] = item;
                 noInventario = true;
                 if (quant>5)
                 {
                     this.quant[x] =5;
                 }
                 else{
                     this.quant[x] = quant;
                 }
             }
         }
         quantItensInventario +=1; 
    }
    
    public void equipar(int nItem){
        boolean equipado = false; 
        String equipar = getNomeItem(nItem);
        for (int x=0; x<limiteEquipados && equipado==false; x++){
            if ("".equals(itensEquipados[x])){
                itensEquipados[x] = equipar;
                quantItensEquipados+=1;
                quant[nItem] -=1;
                equipado=true;
            }
        }        
    }
            
    public String getNomeEquipado (int nItem){
        return itensEquipados[nItem];
    }
    public String getNomeItem(int nItem){
        return itens[nItem];
    }
    public int getQuantItem (int nItem){
        return quant[nItem];
    }
    
    public boolean setItem (int quant, int nItem, String nome){
        itens[nItem] = nome;
        if (!"".equals(itens[nItem])){
            
            if ((this.quant [nItem]+quant) >=limiteInventario){
                this.quant[nItem] = limiteInventario;
                return true;
            }
            else{
                this.quant[nItem] += quant;
                return true;
            }
        }
        else {
            return false;
        }
    }
   
    public int getLimiteInventario(){
        return this.limiteInventario;
    }
    
}
