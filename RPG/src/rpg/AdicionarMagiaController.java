package rpg;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class AdicionarMagiaController implements Initializable {

    int quant = 0;
    ArrayList <Label> labels = new ArrayList<>();
    ArrayList <TextField> nomes = new ArrayList<>();
    ArrayList <TextField> danos = new ArrayList<>();
    ArrayList <TextField> manas = new ArrayList<>();
    ArrayList<Jogador> vetJogador = new ArrayList<>();
    Jogador jogador;
    
    @FXML
    private AnchorPane tela;
    
    @FXML
    private Label aviso;
    @FXML
    private TextField nome1,dano1, mana1;
    
    
    public void setJogadores(ArrayList<Jogador> vet, Jogador nJogador){
        vetJogador = vet;
        jogador = nJogador;
        
        if (jogador.getSizeMagia()>=1){
            nome1.setText(jogador.getMagia(0));
            dano1.setText(Integer.toString(jogador.getDano(0)));
            mana1.setText(Integer.toString(jogador.getConsumoMana(0)));
            if (jogador.getSizeMagia()>1){
                for (int x=1;x<jogador.getSizeMagia();x++){
                    adicionar();
                    nomes.get(x-1).setText(jogador.getMagia(x));
                    danos.get(x-1).setText(Integer.toString(jogador.getDano(x)));
                    manas.get(x-1).setText(Integer.toString(jogador.getConsumoMana(x)));
                }
            }
        }
    }
    
    @FXML
    private void salvarMagias(){
        String nome = nome1.getText();
        String dano = dano1.getText();
        String mana = mana1.getText();
        int danoa = Integer.parseInt(dano);
        int manaa = Integer.parseInt(mana);
        if (!"".equals(nome) && !"".equals(dano) && !"".equals(mana)&& danoa<=5 && manaa<=5){
            jogador.adicionarMagia(nome1.getText(), dano1.getText(), mana1.getText());
            RPG.trocaTela("Menu.fxml", vetJogador, jogador);
        }
        else{
            aviso.setText("Preencha os campos (com valores até 5) para salvar!");
        }
        for (int x=0;x<quant && x>=0;x++){
            nome = nomes.get(x).getText();
            dano = danos.get(x).getText();
            mana = manas.get(x).getText();
            
            if (!"".equals(nome) && !"".equals(dano) && !"".equals(mana) && danoa<=5 && manaa<=5){
                jogador.adicionarMagia(nome, dano, mana);                
                RPG.trocaTela("Menu.fxml", vetJogador, jogador);
            }
            else{
                aviso.setText("Preencha os campos (com valores até 5) para salvar!");
            }
        }
        
    }
    
    @FXML
    private void adicionar(){
        aviso.setText (" ");
        int posY =126 ;
        for (int x =0; x<quant+1;x++){
            posY +=40;
        }
        
        Label lb = new Label("Magia "+ (quant+2)+":");
        lb.setLayoutX(28);
        lb.setLayoutY(posY);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        labels.add(lb) ;
        
        TextField nome = new TextField();
        nome.setLayoutX(94);
        nome.setLayoutY(posY);
        nome.setPromptText("Insira o nome");
        nome.setMaxWidth(91);
        nome.setId("nome"+(quant+2));
        nomes.add(nome);
        
        TextField dano = new TextField();    
        dano.setLayoutX(197);
        dano.setLayoutY(posY);
        dano.setPromptText("Dano");
        dano.setMaxWidth(85);
        dano.setId("dano"+(quant+2));
        danos.add(dano);
        
        TextField mana = new TextField();    
        mana.setLayoutX(298);
        mana.setLayoutY(posY);
        mana.setPromptText("Consumo Mana");
        mana.setMaxWidth(101);
        mana.setId("mana"+(quant+2));
        manas.add(mana);
            
        tela.getChildren().addAll (lb,nome,dano,mana);
        quant ++;
    }
    @FXML
    private void remover(){
        
        aviso.setText ("");
        if (quant>=1){
            quant--;

            tela.getChildren().remove (labels.get(quant));
            tela.getChildren().remove (nomes.get(quant));
            tela.getChildren().remove (danos.get(quant));
            tela.getChildren().remove (manas.get(quant));
            labels.remove(quant);
            nomes.remove (quant);
            danos.remove(quant);
            manas.remove(quant);
        }
        else{
            aviso.setText("Você não pode ter menos de uma magia!");
        }
        
    }
    
    @FXML
    private void voltar(){
        RPG.trocaTela("Menu.fxml", vetJogador, jogador);
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
    }    

    
}
