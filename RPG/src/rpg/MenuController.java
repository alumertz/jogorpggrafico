package rpg;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MenuController implements Initializable {
    
    ArrayList<Jogador> vetJogador = new ArrayList<>();
    Jogador jogador;
    @FXML
    private Label aviso,lJogador,magia;
    @FXML
    private TextField nomeAdicionar;
    @FXML
    private ChoiceBox nomeEscolher = new ChoiceBox(), nomeMagia= new ChoiceBox();
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> MList = new ArrayList<>();
    ObservableList<String>oList = FXCollections.observableList(list);
    ObservableList<String>oMList = FXCollections.observableList(MList);
    
    public void set(ArrayList<Jogador> vet, Jogador nJogador){
        vetJogador = vet;
        jogador = nJogador;
        lJogador.setText(nJogador.getNome());
        for (int x=0;x<vetJogador.size();x++){
            list.add(vetJogador.get(x).getNome());
        }
        for (int x=0; x<jogador.getSizeMagia();x++){
            MList.add(jogador.getMagia(x));
        }        
        nomeMagia.setItems(oMList);
        nomeEscolher.setItems(oList);
        
    }
    @FXML
    void editarEquipados() {
        RPG.trocaTela("EditarEquipados.fxml",vetJogador, jogador);
    }
    @FXML
    void editarInventario() {
        RPG.trocaTela("EditarInventario.fxml",vetJogador, jogador);
    }
    @FXML
    void adicionarMagia(){
        RPG.trocaTela("AdicionarMagia.fxml",vetJogador, jogador);
    }
    @FXML
    void executarMagia() {
        String opcao = nomeMagia.getSelectionModel().getSelectedItem().toString();
        for (int x=0; x<jogador.getSizeMagia();x++){
            if (jogador.getMagia(x).equals(opcao)){
                jogador.executarMagia(x);
                magia.setText(jogador.toString());
            }
        }
    }
    @FXML
    void adicionarJogador(){
        String nome = nomeAdicionar.getText();
        vetJogador.add(new Jogador (nome));
        lJogador.setText(nome);
        jogador = vetJogador.get(vetJogador.size()-1);
        list.add(nome);
        nomeEscolher.setItems(FXCollections.observableArrayList());
        nomeEscolher.setItems(oList);
    }
    @FXML
    void mostrarMagias(){
        nomeMagia.show();
    }
    @FXML
    void mostrarJogador(){
        nomeEscolher.show();
    }
    @FXML
    void escolherJogador(){
        
        String opcaoEscolhida = nomeEscolher.getSelectionModel().getSelectedItem().toString();
        for (int x=0; x<vetJogador.size();x++){
            if (vetJogador.get(x).getNome().equals(opcaoEscolhida)){
                jogador = vetJogador.get(x);
                lJogador.setText(jogador.getNome());
            }
        }
    }
    public void primeiro (){
        vetJogador.add(new Jogador("Primeiro"));
        lJogador.setText("Primeiro");
        jogador = vetJogador.get(0);
        
        list.add("Primeiro");
        nomeEscolher.setItems(oList);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        
    }    
    
}
