
package rpg;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RPG extends Application {
    public static MenuController controllerMenu;
    public static EditarInventarioController controllerInventario;
    public static EditarEquipadosController controllerEquipados;
    public static AdicionarMagiaController controllerAddMagia;
    
    private static Stage stage;
    
    public static Stage getStage() {
        return stage;
    }
     public static void trocaTela(String tela, ArrayList<Jogador> vetJogador ,Jogador jogador){
        Parent root = null;
        try{
            FXMLLoader loader = new FXMLLoader(RPG.class.getResource(tela));
            root = loader.load();
            Scene scene = new Scene(root);
            
            switch (tela){                
               case "Menu.fxml":
                    controllerMenu = (MenuController)loader.getController();
                    controllerMenu.set(vetJogador, jogador);
                    break;
                case "EditarInventario.fxml":
                    controllerInventario = (EditarInventarioController)loader.getController();
                    controllerInventario.setJogadores(vetJogador, jogador);
                    break;
                case "EditarEquipados.fxml":
                    controllerEquipados = (EditarEquipadosController)loader.getController();
                    controllerEquipados.setJogadores(vetJogador, jogador);
                    break;
                case "AdicionarMagia.fxml":
                    controllerAddMagia = (AdicionarMagiaController)loader.getController();
                    controllerAddMagia.setJogadores(vetJogador, jogador);
                    break;
                
            }
                       
            stage.setScene(scene);
            stage.show();
        }
        catch(Exception e){
            System.out.println("Verificar arquivo FXML " + e);
            e.printStackTrace();
        }        
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(RPG.class.getResource("Menu.fxml"));
        root = loader.load();
        Scene scene = new Scene(root);
        
        controllerMenu = (MenuController)loader.getController();
        controllerMenu.primeiro();
        
        RPG.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    
    public static void main(String[] args) {
        launch(args);
    }
    
}
